﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ufmt.sig.entity;
using ufmt.sig.business;

namespace ufmt.gcdh.Controllers
{
    public class PessoaController : Controller
    {
        //
        // GET: /Pessoa/

        internal Pessoa GetPessoa(long pessoaUID)
        {
            using (IPessoaBO pessoaBO = BusinessFactory.GetInstance<IPessoaBO>())
            {
                return pessoaBO.GetPessoa(pessoaUID);
            }
        }

        internal Servidor GetServidor(long pessoaUID)
        {
            using (IPessoaBO pessoaBO = BusinessFactory.GetInstance<IPessoaBO>())
            {
                return pessoaBO.GetVinculosPorPessoa(pessoaUID, true).First();
            }
        }

        internal bool VerificaJulioMuller(Unidade unidade)
        {
            if (unidade.UnidadeSuperior == null)
            {
                return false;
            } 
            else if (unidade.UnidadeSuperior.UnidadeUID == (int)ufmt.gcdh.Models.Polo.HUJM)
            {
                return true;
            }

            unidade = unidade.UnidadeSuperior;

            return this.VerificaJulioMuller(unidade);
        }

        internal ufmt.gcdh.Models.Polo GetPoloServidor(Servidor servidor)
        {
            ufmt.gcdh.Models.Polo polo = ufmt.gcdh.Models.Polo.Cuiaba;
            switch (servidor.UnidadeLotacao.Campus.CampusUID)
            {
                case (int)ufmt.gcdh.Models.Polo.Cuiaba:
                    polo = ufmt.gcdh.Models.Polo.Cuiaba;
                    break;
                case (int)ufmt.gcdh.Models.Polo.Sinop:
                    polo = ufmt.gcdh.Models.Polo.Sinop;
                    break;
                case (int)ufmt.gcdh.Models.Polo.Rondonopolis:
                    polo = ufmt.gcdh.Models.Polo.Rondonopolis;
                    break;
                case (int)ufmt.gcdh.Models.Polo.Araguaia:
                    polo = ufmt.gcdh.Models.Polo.Araguaia;
                    break;
            }

            return polo;

        }
    }
}
