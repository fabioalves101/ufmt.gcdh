﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ufmt.gcdh.Models;
using System.Web.Configuration;

namespace ufmt.gcdh.Controllers
{
    [Authorize(Roles = "Candidato")]
    public class InscricaoController : BaseController
    {
        private idbufmtEntities db = new idbufmtEntities();

        //
        // GET: /Inscricao/

        public ViewResult Index()
        {
            var inscricao = db.Inscricao.Include(i => i.Pessoa).Include(i => i.Turma);
            return View(inscricao.ToList());
        }

        //
        // GET: /Inscricao/Details/5

        public ViewResult Details(int id)
        {
            Inscricao inscricao = db.Inscricao.Find(id);
            return View(inscricao);
        }

        //
        // GET: /Inscricao/Create

        public ActionResult Create()
        {
            ViewBag.pessoaUID = new SelectList(db.Pessoa, "pessoaUID", "nome");
            ViewBag.turmaUID = new SelectList(db.Turma, "turmaUID", "nome");
            return View();
        } 

        //
        // POST: /Inscricao/Create

        [HttpPost]
        public ActionResult Create(Inscricao inscricao)
        {
            if (ModelState.IsValid)
            {
                db.Inscricao.Add(inscricao);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            ViewBag.pessoaUID = new SelectList(db.Pessoa, "pessoaUID", "nome", inscricao.pessoaUID);
            ViewBag.turmaUID = new SelectList(db.Turma, "turmaUID", "nome", inscricao.turmaUID);
            return View(inscricao);
        }

        public ActionResult Salvar(Inscricao inscricao)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Turma turma = db.Turma.Find(inscricao.turmaUID);
                    if (new CursoController()
                            .IsUnique(inscricao.pessoaUID.Value, turma.Curso.cursoUID))
                    {
                        List<Inscricao> inscricoes = db.Inscricao.Where(c => c.turmaUID == inscricao.turmaUID).ToList();
                        if (inscricoes.Count < turma.quantidadeVagas)
                        {

                            db.Inscricao.Add(inscricao);
                            db.SaveChanges();

                            return RedirectToAction("details", new { id = inscricao.inscricaoUID });
                        }
                        else
                        {
                            int id = int.Parse(WebConfigurationManager.AppSettings["formUID"].ToString());

                            return RedirectToAction("Index", "Curso", new { id = id, error = 1002 });
                            //return RedirectToAction("Index", "Turma", new { id = turma.cursoUID });
                        }
                    }
                    else
                    {
                        inscricao = db.Inscricao.Where(t => t.turmaUID == inscricao.turmaUID)
                                        .Where(p => p.pessoaUID == inscricao.pessoaUID)
                                        .First();

                        return RedirectToAction("details", new { id = inscricao.inscricaoUID });
                    }
                }
                else
                {
                    return RedirectToAction("index", "turma", new { id = inscricao.turmaUID });
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }
        
        //
        // GET: /Inscricao/Edit/5
 
        public ActionResult Edit(int id)
        {
            Inscricao inscricao = db.Inscricao.Find(id);
            ViewBag.pessoaUID = new SelectList(db.Pessoa, "pessoaUID", "nome", inscricao.pessoaUID);
            ViewBag.turmaUID = new SelectList(db.Turma, "turmaUID", "nome", inscricao.turmaUID);
            return View(inscricao);
        }

        //
        // POST: /Inscricao/Edit/5

        [HttpPost]
        public ActionResult Edit(Inscricao inscricao)
        {
            if (ModelState.IsValid)
            {
                db.Entry(inscricao).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.pessoaUID = new SelectList(db.Pessoa, "pessoaUID", "nome", inscricao.pessoaUID);
            ViewBag.turmaUID = new SelectList(db.Turma, "turmaUID", "nome", inscricao.turmaUID);
            return View(inscricao);
        }

        //
        // GET: /Inscricao/Delete/5
 
        public ActionResult Delete(int id)
        {
            Inscricao inscricao = db.Inscricao.Find(id);
            return View(inscricao);
        }

        //
        // POST: /Inscricao/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {            
            Inscricao inscricao = db.Inscricao.Find(id);
            db.Inscricao.Remove(inscricao);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }       
    }
}