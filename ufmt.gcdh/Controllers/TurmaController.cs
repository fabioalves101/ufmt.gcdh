﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ufmt.gcdh.Models;

namespace ufmt.gcdh.Controllers
{
    [Authorize(Roles = "Candidato")]
    public class TurmaController : BaseController
    {
        private idbufmtEntities db = new idbufmtEntities();

        //
        // GET: /Turma/

        public ActionResult Index(int id)
        {
            try
            {
                long pessoaUID = long.Parse(SimpleSessionPersister.Id);
                List<Turma> turma = this.GetListTurma(pessoaUID, id, true);

                if (turma.Count > 0)
                    return View(turma);
                else
                {
                    TempData["message"] = "Não existe curso com o código especificado.";
                    return View(new List<Turma>());
                }
            }
            catch 
            {
                return RedirectToAction("logoff", "account");
            }
        }

        [HttpPost]
        public ActionResult SelecionarTurma(Turma turma)
        {
            var vturma = db.Turma.Find(turma.turmaUID);

            long pessoaUID = long.Parse(SimpleSessionPersister.Id);

            return RedirectToAction("salvar", "inscricao", new Inscricao
            {
                pessoaUID = pessoaUID,
                turmaUID = vturma.turmaUID,
                listaEspera = false,
                dataInscricao = DateTime.Now
            });
        }

        private List<Turma> GetListTurma(long pessoaUID, int cursoUID, bool apenasAtivas)
        {          
            PessoaController pController = new PessoaController();
            ufmt.sig.entity.Servidor servidor = pController.GetServidor(pessoaUID);

            List<Turma> turma = new List<Turma>();
            if (servidor.UnidadeLotacao != null)
            {
                if (servidor.UnidadeLotacao.Campus.CampusUID == (int)Polo.Cuiaba)
                {
                    if (pController.VerificaJulioMuller(servidor.UnidadeLotacao))
                    {
                        turma = db.Turma.Include(t => t.Curso)
                            .Where(c => c.cursoUID == cursoUID)
                            .Where(p => p.poloUID == (int)Polo.HUJM).ToList();
                    }
                    else
                    {
                        turma = db.Turma.Include(t => t.Curso)
                            .Where(c => c.cursoUID == cursoUID)
                            .Where(p => p.poloUID == (int)Polo.Cuiaba).ToList();
                    }
                }
                else
                {
                    int poloUID = (int)pController.GetPoloServidor(servidor);
                                         
                        turma = db.Turma.Include(t => t.Curso)
                                .Where(c => c.cursoUID == cursoUID)
                                .Where(p => p.poloUID == poloUID)
                                .ToList();
                }
            }
            else
            {
                turma = db.Turma.Include(t => t.Curso).Where(c => c.cursoUID == cursoUID).ToList();
            }

            if (apenasAtivas)
                turma = turma.Where(t => t.ativo == true).ToList();

            return turma;
        }

        //
        // GET: /Turma/Details/5

        public ViewResult Details(int id)
        {
            Turma turma = db.Turma.Find(id);
            return View(turma);
        }

        //
        // GET: /Turma/Create

        public ActionResult Create()
        {
            ViewBag.cursoUID = new SelectList(db.Curso, "cursoUID", "nome");
            return View();
        } 

        //
        // POST: /Turma/Create

        [HttpPost]
        public ActionResult Create(Turma turma)
        {
            if (ModelState.IsValid)
            {
                db.Turma.Add(turma);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            ViewBag.cursoUID = new SelectList(db.Curso, "cursoUID", "nome", turma.cursoUID);
            return View(turma);
        }
        
        //
        // GET: /Turma/Edit/5
 
        public ActionResult Edit(int id)
        {
            Turma turma = db.Turma.Find(id);
            ViewBag.cursoUID = new SelectList(db.Curso, "cursoUID", "nome", turma.cursoUID);
            return View(turma);
        }

        //
        // POST: /Turma/Edit/5

        [HttpPost]
        public ActionResult Edit(Turma turma)
        {
            if (ModelState.IsValid)
            {
                db.Entry(turma).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.cursoUID = new SelectList(db.Curso, "cursoUID", "nome", turma.cursoUID);
            return View(turma);
        }

        //
        // GET: /Turma/Delete/5
 
        public ActionResult Delete(int id)
        {
            Turma turma = db.Turma.Find(id);
            return View(turma);
        }

        //
        // POST: /Turma/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {            
            Turma turma = db.Turma.Find(id);
            db.Turma.Remove(turma);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}