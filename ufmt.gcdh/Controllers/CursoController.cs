﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ufmt.gcdh.Models;
using System.Web.Configuration;

namespace ufmt.gcdh.Controllers
{
    [Authorize(Roles = "Candidato")]
    public class CursoController : BaseController
    {
        private idbufmtEntities db = new idbufmtEntities();

        //
        // GET: /Curso/

        public ViewResult Index(int id, int? error = null)
        {
            ViewBag.ErrorMessage = String.Empty;                
            if (error == 1001)
                ViewBag.ErrorMessage = "O usuário já realizou a inscrição nesse curso.";            
            if(error == 1002)
                ViewBag.ErrorMessage = "A quantidade de inscrições foi excedida.";            
            //var curso = db.Curso.Include(c => c.Formulario).Where(c => c.formularioUID == id);

            id = int.Parse(WebConfigurationManager.AppSettings["formUID"].ToString());

            var curso =
            (from c in db.Curso
             join f in db.Formulario on c.formularioUID equals f.formularioUID
             where c.formularioUID == id && f.ativo == true
             select c).Include(c => c.Formulario);

            List<Curso> listCurso = new List<Curso>();
            foreach (Curso c in curso)
            {
                if (IsCursoTurmaAtiva(c.cursoUID))
                {
                    listCurso.Add(c);
                }
            }                        

            return View(listCurso);
        }

        private bool IsCursoTurmaAtiva(int cursoUID)
        {
            bool ativo = false;
            Curso curso = this.db.Curso.Where(c => c.cursoUID == cursoUID).FirstOrDefault();
            foreach (Turma t in curso.Turma)
            {
                if (t.ativo.HasValue && t.ativo.Value)
                {
                    ativo = true;
                }
            }

            return ativo;
        }

        public ActionResult SelecionarCurso(int id, int form)
        {
            //int limiteInscricoes = int.Parse(WebConfigurationManager.AppSettings["limiteInscricoes"].ToString());
            int limiteInscricoes = 10000;
            if(this.db.Curso.Find(id).limiteInscricoes.HasValue)
                limiteInscricoes = this.db.Curso.Find(id).limiteInscricoes.Value;
                
            if (!this.IsLimiteInscricoesAtingido(id, limiteInscricoes))
            {
                if (IsUnique(long.Parse(SimpleSessionPersister.Id), id))
                {
                    return RedirectToAction("index", "turma", new { id = id });
                }
                else
                {
                    return RedirectToAction("index", new { id = form, error = 1001 });
                }
            }
            else
            {
                return RedirectToAction("index", new { id = form, error = 1002 });
            }
        }

        private bool IsLimiteInscricoesAtingido(int cursoUID, int limiteInscricoes)
        {
            bool limitaQuantidadeInscricoes = bool.Parse(WebConfigurationManager.AppSettings["limitaQuantidadeInscricoes"].ToString());
            if (limitaQuantidadeInscricoes)
            {
                int quantidadeInscritos =
                    (from i in this.db.Inscricao
                     join t in this.db.Turma on i.turmaUID equals t.turmaUID
                     join c in this.db.Curso on t.cursoUID equals c.cursoUID
                     where c.cursoUID == cursoUID
                     select i).Count();

                if (quantidadeInscritos >= limiteInscricoes)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public bool IsUnique(long pessoaUID, int cursoUID)
        {
            List<Inscricao> listInscricao =
                    (from i in db.Inscricao
                     join t in db.Turma on i.turmaUID equals t.turmaUID
                     join c in db.Curso on t.cursoUID equals c.cursoUID
                     where i.pessoaUID == pessoaUID && c.cursoUID == cursoUID
                     select i).ToList();

            if (listInscricao.Count > 0)
                return false;
            else
                return true;
        }

        //
        // GET: /Curso/Details/5

        public ViewResult Details(int id)
        {
            Curso curso = db.Curso.Find(id);
            return View(curso);
        }

        //
        // GET: /Curso/Create

        public ActionResult Create()
        {
            ViewBag.formularioUID = new SelectList(db.Formulario, "formularioUID", "titulo");
            return View();
        } 

        //
        // POST: /Curso/Create

        [HttpPost]
        public ActionResult Create(Curso curso)
        {
            if (ModelState.IsValid)
            {
                db.Curso.Add(curso);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            ViewBag.formularioUID = new SelectList(db.Formulario, "formularioUID", "titulo", curso.formularioUID);
            return View(curso);
        }
        
        //
        // GET: /Curso/Edit/5
 
        public ActionResult Edit(int id)
        {
            Curso curso = db.Curso.Find(id);
            ViewBag.formularioUID = new SelectList(db.Formulario, "formularioUID", "titulo", curso.formularioUID);
            return View(curso);
        }

        //
        // POST: /Curso/Edit/5

        [HttpPost]
        public ActionResult Edit(Curso curso)
        {
            if (ModelState.IsValid)
            {
                db.Entry(curso).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.formularioUID = new SelectList(db.Formulario, "formularioUID", "titulo", curso.formularioUID);
            return View(curso);
        }

        //
        // GET: /Curso/Delete/5
 
        public ActionResult Delete(int id)
        {
            Curso curso = db.Curso.Find(id);
            return View(curso);
        }

        //
        // POST: /Curso/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {            
            Curso curso = db.Curso.Find(id);
            db.Curso.Remove(curso);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }

}