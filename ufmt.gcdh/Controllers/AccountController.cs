﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using ufmt.gcdh.Models;
using ufmt.gcdh.ViewModel;
using ufmt.sig.business;
using ufmt.sig.entity;

namespace ufmt.gcdh.Controllers
{
    public class AccountController : Controller
    {
        //
        // GET: /Account/LogOn

        public ActionResult LogOn()
        {
            return View();
        }

        //
        // POST: /Account/LogOn

        [HttpPost]
        public ActionResult LogOn(LogOnModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                if (Membership.ValidateUser(model.UserName, model.Password))
                {
                    FormsAuthentication.SetAuthCookie(model.UserName, model.RememberMe);
                    if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/")
                        && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
                    {
                        return Redirect(returnUrl);
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "The user name or password provided is incorrect.");
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/LogOff

        public ActionResult LogOff()
        {
            SimpleSessionPersister.Username = null;

            return RedirectToAction("Index", "Home");
        }

        //
        // GET: /Account/Register

        public ActionResult Register()
        {
            return View();
        }

        //
        // POST: /Account/Register

        [HttpPost]
        public ActionResult Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                // Attempt to register the user
                MembershipCreateStatus createStatus;
                Membership.CreateUser(model.UserName, model.Password, model.Email, null, null, true, null, out createStatus);

                if (createStatus == MembershipCreateStatus.Success)
                {
                    FormsAuthentication.SetAuthCookie(model.UserName, false /* createPersistentCookie */);
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("", ErrorCodeToString(createStatus));
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ChangePassword

        [Authorize]
        public ActionResult ChangePassword()
        {
            return View();
        }

        //
        // POST: /Account/ChangePassword

        [Authorize]
        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordModel model)
        {
            if (ModelState.IsValid)
            {

                // ChangePassword will throw an exception rather
                // than return false in certain failure scenarios.
                bool changePasswordSucceeded;
                try
                {
                    MembershipUser currentUser = Membership.GetUser(User.Identity.Name, true /* userIsOnline */);
                    changePasswordSucceeded = currentUser.ChangePassword(model.OldPassword, model.NewPassword);
                }
                catch (Exception)
                {
                    changePasswordSucceeded = false;
                }

                if (changePasswordSucceeded)
                {
                    return RedirectToAction("ChangePasswordSuccess");
                }
                else
                {
                    ModelState.AddModelError("", "The current password is incorrect or the new password is invalid.");
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ChangePasswordSuccess

        public ActionResult ChangePasswordSuccess()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Login(string formUID)
        {
            if (string.IsNullOrEmpty(formUID))
            {
                formUID = "10";
            }

            ViewBag.formUID = formUID;
            return View();

        }

        [HttpPost]
        public ActionResult Login(LoginPageViewModel viewModel, string formUID = null)
        {
            try
            {
                if (formUID == null)
                    formUID = 10.ToString();


                if (string.IsNullOrEmpty(viewModel.UserName) || string.IsNullOrEmpty(viewModel.Password))
                {
                    ViewBag.error = "Por favor forneça o nome de usuário e senha.";
                    return View();
                }

                Usuario usuario = GetUsuario(viewModel.UserName, viewModel.Password);
                if (usuario != null)
                {
                    if (VerificaAutenticacaoValida(usuario))
                    {
                        SimpleSessionPersister.Username = usuario.Pessoa.Nome;
                        SimpleSessionPersister.Id = usuario.Pessoa.PessoaUID.ToString();

                        return RedirectToAction("Index", "Curso", new { id = int.Parse(formUID) });
                    }
                    else
                    {
                        ViewBag.error = "O usuário não é valido para essa aplicação.";
                        return View();
                    }
                }
                else
                {
                    ViewBag.error = "Usuário e senha inválidos";
                    return View();
                }
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View();

            }
        }

        private bool VerificaAutenticacaoValida(Usuario usuario)
        {
            using (IPessoaBO pessoaBO = BusinessFactory.GetInstance<IPessoaBO>())
            {
                IList<ufmt.sig.entity.Servidor> listServidor = 
                    pessoaBO.GetVinculosPorPessoa(usuario.Pessoa.PessoaUID, false);


                bool valido = false;

                foreach (ufmt.sig.entity.Servidor servidor in listServidor)
                {
                    if (servidor.SituacaoServidor.SituacaoServidorUID == 1)
                    {
                        valido = true;
                        break;
                    }
                    else
                    {
                        if (servidor.AposentadoriaNumeroProcesso != null)
                        {
                            valido = true;
                            break;
                        }
                    }
                }

                return valido;
            }
        }

        private Usuario GetUsuario(string cpf, string senha)
        {
            try
            {
                using (IAutenticacaoBO autenticacaoBO = BusinessFactory.GetInstance<IAutenticacaoBO>())
                {
                    Usuario usuario = autenticacaoBO.GetUsuario(cpf, senha);
                    return usuario;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Não foi possível realizar o acesso. " + ex.Message);
            }
        }



        #region Status Codes
        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "User name already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }
        #endregion
    }
}
